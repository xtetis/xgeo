<?php

/**
 * Рендер списка профилей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$parent = strval($_REQUEST['parent']);

$data = [];

$parent = str_replace('select_multilevel_', '', $parent);

$region_selectable  = \xtetis\xengine\helpers\RequestHelper::get('region_selectable', 'int', 0);
$country_selectable = \xtetis\xengine\helpers\RequestHelper::get('country_selectable', 'int', 0);

$data = [];
if ('#' == $parent)
{
    $model_country_root = new \xtetis\xgeo\models\CountryModel();
    foreach ($model_country_root->getCountryModelList() as $id_country => $model_country)
    {
        $data[] = [
            'id'       => 'select_multilevel_country_' . $model_country->id,
            'text'     => $model_country->name,
            'li_attr'  => [
                'is_selectable' => ($country_selectable ? '1' : '0'),
                'idx'           => $model_country->id,
                'name'          => $model_country->name,
                'level'         => 1,
            ],
            'children' => true,
            'type'     => 'root',
        ];
    }
}
else
{
    $parent_arr = explode('_', $parent);
    if (count($parent_arr) == 2)
    {
        $type = strval($parent_arr[0]);
        $id   = intval($parent_arr[1]);
        if ('country' == $type)
        {
            $model_country = \xtetis\xgeo\models\CountryModel::generateModelById($id);
            if ($model_country)
            {
                foreach ($model_country->getRegionModelList() as $id_region => $model_region)
                {
                    $data[] = [
                        'id'       => 'select_multilevel_region_' . $model_region->id,
                        'text'     => $model_region->name,
                        'li_attr'  => [
                            'is_selectable' => ($region_selectable ? '1' : '0'),
                            'idx'           => $model_region->id,
                            'name'          => $model_country->name . ' -> ' . $model_region->name,
                            'level'         => 2,
                        ],
                        'children' => true,
                    ];
                }
            }
        }
        elseif ('region' == $type)
        {
            $model_region = \xtetis\xgeo\models\RegionModel::generateModelById($id);
            if ($model_region)
            {
                $model_country = $model_region->getModelRelated('id_country');
                if ($model_country)
                {
                    foreach ($model_region->getCityModelList() as $id_region => $model_city)
                    {
                        $data[] = [
                            'id'       => 'select_multilevel_city_' . $model_city->id,
                            'text'     => $model_city->name,
                            'li_attr'  => [
                                'is_selectable' => '1',
                                'idx'           => $model_city->id,
                                'name'          => $model_country->name . ' -> ' . $model_region->name . ' -> ' . $model_city->name,
                                'level'         => 3,

                            ],
                            'children' => false,
                        ];
                    }
                }
            }
        }
    }
}

header('Content-type: text/json');
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
echo json_encode($data);
