<?php

namespace xtetis\xgeo\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class RegionModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xgeo_region';

    /**
     * ID
     */
    public $id = '';

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $id_country = 0;

    /**
     * @var array
     */
    public $region_list = [];

    /**
     * Список моделей городов, привязанных к региону
     */
    public $city_model_list = [];

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     * 
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_country' => \xtetis\xgeo\models\CountryModel::class
    ];

    /**
     * @return mixed
     */
    public function getRegionList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_country = intval($this->id_country);

        $rows = \xtetis\xgeo\models\SqlModel::getRegionList(
            $this->id_country
        );

        if ($rows)
        {
            foreach ($rows as $row)
            {
                $this->region_list[intval($row['id'])] = $row['name'];
            }
        }

        return $this->region_list;

    }

    /**
     * Возвращает список категорий статей в виде опций для select
     */
    public function getOptions(
        $add_root = true
    )
    {

        $ret = [];

        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->region_list)
        {
            $this->getRegionList();
            
            if ($this->getErrors())
            {
                return false;
            }
        }

        if ($add_root)
        {
            $ret[0] = 'Не указано';
        }

        foreach ($this->region_list as $k => $v)
        {
            $ret[$k] = $v;
        }

        return $ret;
    }




    /**
     * Возвращает список моделей городов для указанного региона
     */
    public function getCityModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->city_model_list)
        {
            return $this->city_model_list;
        }


        $sql = '
            SELECT id
            FROM xgeo_city t
            WHERE
            id_region = :id_region
        ';
        

        $params = [
            'id_region'=>$this->id,
        ];

        $this->city_model_list = \xtetis\xgeo\models\CityModel::getModelListBySql($sql, $params);

        return $this->city_model_list;
    }
}
