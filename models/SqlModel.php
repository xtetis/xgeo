<?php

namespace xtetis\xgeo\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{


    /**
     * Возвращает список стран
     */
    public static function getCountryList()
    {
 

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
        SELECT *
        FROM xgeo_country
        ORDER BY name
        ";
        $stmt    = $connect->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }


    /**
     * Возвращает список регионов
     */
    public static function getRegionList(
        $id_country = 0
    )
    {
        $id_country = intval($id_country);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
        SELECT *
        FROM xgeo_region
        WHERE id_country = :id_country
        ORDER BY name
        ";
        $stmt    = $connect->prepare($sql);
        $stmt->bindParam('id_country', $id_country, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }



    /**
     * Возвращает список городов
     */
    public static function getCityList(
        $id_region = 0
    )
    {
        $id_region = intval($id_region);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
        SELECT *
        FROM xgeo_city
        WHERE id_region = :id_region
        ORDER BY name
        ";
        $stmt    = $connect->prepare($sql);
        $stmt->bindParam('id_region', $id_region, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }

}
