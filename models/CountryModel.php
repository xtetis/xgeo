<?php

namespace xtetis\xgeo\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class CountryModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xgeo_country';

    /**
     * ID
     */
    public $id = '';

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var array
     */
    public $country_list = [];

    /**
     * Список моделей стран
     */
    public $country_model_list = [];

    /**
     * Список связанных регионов
     */
    public $region_model_list = [];

    /**
     * @return mixed
     */
    public function getCountryList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $rows = \xtetis\xgeo\models\SqlModel::getCountryList();

        if ($rows)
        {
            foreach ($rows as $row)
            {
                $this->country_list[intval($row['id'])] = $row['name'];
            }
        }

        return $this->country_list;

    }

    /**
     * Возвращает список категорий статей в виде опций для select
     */
    public function getOptions(
        $add_root = true
    )
    {

        $ret = [];

        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->country_list)
        {
            $this->getCountryList();
            
            if ($this->getErrors())
            {
                return false;
            }
        }

        if ($add_root)
        {
            $ret[0] = 'Не указано';
        }

        foreach ($this->country_list as $k => $v)
        {
            $ret[$k] = $v;
        }

        return $ret;
    }

    /**
     * Возвращает список моделей стран
     */
    public function getCountryModelList()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if ($this->country_model_list)
        {
            return $this->country_model_list;
        }


        $sql = '
            SELECT id
            FROM xgeo_country t
        ';

        $params = [
        ];

        $this->country_model_list = self::getModelListBySql($sql, $params);

        return $this->country_model_list;
    }

    /**
     * Возвращает список моделей для указанной страны
     */
    public function getRegionModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->region_model_list)
        {
            return $this->region_model_list;
        }


        $sql = '
            SELECT id
            FROM xgeo_region t
            WHERE
            id_country = :id_country
        ';
        

        $params = [
            'id_country'=>$this->id,
        ];

        $this->region_model_list = \xtetis\xgeo\models\RegionModel::getModelListBySql($sql, $params);

        return $this->region_model_list;
    }
}
