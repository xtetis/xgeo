<?php

namespace xtetis\xgeo\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class CityModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xgeo_city';

    /**
     * ID
     */
    public $id = '';

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $id_region = 0;

    /**
     * @var array
     */
    public $city_list = [];

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     * 
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_region' => \xtetis\xgeo\models\RegionModel::class
    ];

    /**
     * @return mixed
     */
    public function getCityList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_region = intval($this->id_region);

        $rows = \xtetis\xgeo\models\SqlModel::getCityList(
            $this->id_region
        );

        if ($rows)
        {
            foreach ($rows as $row)
            {
                $this->city_list[intval($row['id'])] = $row['name'];
            }
        }

        return $this->city_list;

    }

    /**
     * Возвращает список категорий статей в виде опций для select
     */
    public function getOptions(
        $add_root = true
    )
    {

        $ret = [];

        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->city_list)
        {
            $this->getCityList();

            if ($this->getErrors())
            {
                return false;
            }
        }

        if ($add_root)
        {
            $ret[0] = 'Не указано';
        }

        foreach ($this->city_list as $k => $v)
        {
            $ret[$k] = $v;
        }

        return $ret;
    }


}
