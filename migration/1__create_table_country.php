<?php

class Migration extends \xtetis\xengine\models\MigrationModel
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql     = 'SELECT NOW()';
        $stmt    = $connect->prepare($sql);
        $stmt->execute();
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
